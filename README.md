Spring Boot + Thymeleaf
=======================

This project is a simple example of using Thymeleaf with Spring Boot.

## Getting Started

- Clone the repository
- Run the application using `./mvnw spring-boot:run`
- Open your browser and navigate to `http://localhost:8080/todos`

## H2 Console

- The H2 console is available at `http://localhost:8080/h2-console`.
- As a JDBC URL, use `jdbc:h2:mem:todos`. The default username is `sa` with an empty password.

## Reloading Thymeleaf templates

To reload Thymeleaf templates without restarting the application, you can change the following property in `application.properties`:

```properties
spring.thymeleaf.prefix=classpath:/templates/
```

to

```properties
spring.thymeleaf.prefix=file:///path/to/your/templates/
```

where `/path/to/your/templates/` is the absolute path to your templates directory.

Examples:

- On `Windows`: `spring.thymeleaf.prefix=file:///C:/projects/thymeleaf-demo/src/main/resources/templates/`
- On `Unix-based` systems: `spring.thymeleaf.prefix=file:///home/user/projects/thymeleaf-demo/src/main/resources/templates/`

## Reference Documentation

- [Thymeleaf](https://www.thymeleaf.org/)
- [Thymeleaf + Spring](https://www.thymeleaf.org/doc/tutorials/3.1/thymeleafspring.html)
- [Thymeleaf Docs](https://www.thymeleaf.org/doc/tutorials/3.1/usingthymeleaf.html#setting-more-than-one-value-at-a-time)
- [Spring Boot](https://spring.io/projects/spring-boot/)

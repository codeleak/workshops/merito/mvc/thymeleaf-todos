# Todo App

## Project Setup

See README.md for project setup instructions.

## Introduction

- Review the existing code in the project.
- The project contains a `Todo` entity, a `TodoRepository`, and a `TodoController`.
- Most of the code is already implemented. You need to complete the Thymeleaf templates.

## Task 1: Display a List of Todos

Modify the Thymeleaf Template
- See `todos.html` file in `src/main/resources/templates`.
- Use Thymeleaf to display the list of todos and a form to add new todos.

## Task 2: Create and Validate Todo

- Modify `todos.html` to include a form to add new todos.
- Update the `todos.html` template to display validation errors next to the form fields.

## Task 3: Delete Todos

Add Delete Functionality:
- Update `todos.html` to include a delete link next to each todo item.

## Additional Tasks

- Add an option to mark todos as completed.
- Filter the list to show only completed or incomplete tasks.
- Add a due date to the todo items and sort them by date.

package pl.codeleak.samples.thymeleaf;


import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class TodoController {

    private final TodoRepository todoRepository;

    public TodoController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @GetMapping("/todos")
    public String showTodos(Model model) {
        model.addAttribute("todos", todoRepository.findAll());
        model.addAttribute("todo", new Todo());
        return "todos";
    }

    @PostMapping("/addTodo")
    public String addTodo(@ModelAttribute("todo") @Valid Todo todo, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("todos", todoRepository.findAll());
            return "todos";
        }
        todoRepository.save(todo);
        redirectAttributes.addFlashAttribute("created", true);
        return "redirect:/todos";
    }

    @GetMapping("/deleteTodo")
    public String deleteTodo(@RequestParam Long id) {
        todoRepository.deleteById(id);
        return "redirect:/todos";
    }
}

